﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Button PlayButton;
    public Button HelpButton;
    public Button ExitButton;
    public GameObject HelpScreen;

	public void Play ()
    {
        SceneManager.LoadScene("main");
    }

    public void Help ()
    {
        HelpScreen.SetActive(true);
    }

    public void Back ()
    {
        HelpScreen.SetActive(false);
    }

    public void Exit ()
    {
        Application.Quit();
    }
}
