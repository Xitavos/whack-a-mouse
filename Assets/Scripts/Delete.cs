﻿using UnityEngine;
using System.Collections;

public class Delete : MonoBehaviour
{
    public AnimationClip textAnimation;

    void Start ()
    {
        float time = textAnimation.length;

        Destroy(gameObject, time);
	}
}