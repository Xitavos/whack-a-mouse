﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Vector2[] SpawnPositions;
    public GameObject[] Buttons;
    public Text ScoreText;
    public Text GameOverText;
    public GameObject ScorePanel;
    public GameObject GameOverPanel;
    [HideInInspector] public bool isGameOver;

    private int score = 0;
    public float waitTime;
    public float spawnSpeed;
    private List<Vector2> openSpawnPositions = new List<Vector2>();

    void Start ()
    {
        score = 0;

        for (int i = 0; i < SpawnPositions.Length; i++)
        {
            openSpawnPositions.Add(SpawnPositions[i]);
        }

        /*for (int i = 0; i < SpawnPositions.Length; i++)
        {
            Vector3 pos = SpawnPositions[i];
            spawnButton("both", pos, 1f);
        }*/

        StartCoroutine(gameLoop());
	}
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}

    IEnumerator gameLoop ()
    {
        waitTime = 1.0f;
        spawnSpeed = 1.00f;

        while(true)
        {
            int randPos = Random.Range(0, openSpawnPositions.Count);
            Vector3 pos = openSpawnPositions[randPos];


            int randType = Random.Range(0, 5);
            string type = "";

            if (randType == 0 || randType == 1)
                type = "lmb";
            else if (randType == 2 || randType == 3)
                type = "rmb";
            else if (randType == 4)
                type = "both";

            spawnButton(type, pos, spawnSpeed);

            yield return new WaitForSeconds(waitTime);

            openSpawnPositions.RemoveAt(randPos);

            if (openSpawnPositions.Count == 0)
            {
                gameOver();
                break;
            }

            waitTime -= 0.01f;
            spawnSpeed += 0.05f;

            if (waitTime <= 0.2f)
                waitTime = 0.2f;
        }
    }

    void spawnButton (string type, Vector3 pos, float spawnTime)
    {
        GameObject newButton;
        ButtonManager buttonManager;

        if (type == "lmb")
            newButton = Instantiate(Buttons[0], pos, Quaternion.identity) as GameObject;
        else if (type == "rmb")
            newButton = Instantiate(Buttons[1], pos, Quaternion.identity) as GameObject;
        else
            newButton = Instantiate(Buttons[2], pos, Quaternion.identity) as GameObject;

        buttonManager = newButton.GetComponent<ButtonManager>();
        buttonManager.type = type;
        buttonManager.pos = pos;
        buttonManager.ChangeSize(spawnTime, true);
    }

    public void ChangeScore(int amount)
    {
        score += amount;

        string newText = "Score: " + score;

        ScoreText.text = newText;
    }

    public void OpenPosition(Vector2 pos)
    {
        openSpawnPositions.Add(pos);
    }

    void gameOver ()
    {
        isGameOver = true;
        ScorePanel.SetActive(false);

        GameOverText.text = "Game Over! Score: " + score;
        GameOverPanel.SetActive(true);

        Invoke("loadMenu", 5f);
    }

    void loadMenu ()
    {
        SceneManager.LoadScene("menu");
    }
}
