﻿using UnityEngine;
using System.Collections;

public class ButtonManager : MonoBehaviour
{
    public string type = "";
    [HideInInspector] public Vector2 pos;
    public GameObject PlusOneText;
    public GameObject MinusOneText;

    private Vector3 start;
    private Vector3 end;
    private float speed = 1.0f;
    private float startTime;
    private float journeyLength;
    private GameManager gameManager;
    private Collider2D thisCollider;

    void Start ()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        thisCollider = GetComponent<Collider2D>();

        //ChangeSize(1f, true);
    }

    void Update()
    {
        if (gameManager.isGameOver)
            thisCollider.enabled = false;

        if (transform.localScale != end)
        {
            float distCovered = (Time.time - startTime) * speed;
            float fracJourney = distCovered / journeyLength;
            transform.localScale = Vector3.Lerp(start, end, fracJourney);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_speed"></param>
    /// <param name="upDown">True is Up, False is Down</param>
	public void ChangeSize (float _speed, bool upDown)
    {
        speed = _speed;

        if(upDown)
        {
            start = transform.localScale;
            end = new Vector3(0.8f, 0.8f, 1.0f);
        }
        else
        {
            start = transform.localScale;
            end = new Vector3(0.0f, 0.0f, 1.0f);
        }

        startTime = Time.time;
        journeyLength = Vector3.Distance(start, end);
	}

    void OnMouseOver ()
    {
        if (Input.GetMouseButton(0) && Input.GetMouseButton(1) && type == "both") //both click
        {
            deleteButton(true);
        }

        if (Input.GetMouseButtonDown(0) && !Input.GetMouseButton(1)) //left click
        {
            if (type == "lmb")
            {
                deleteButton(true);
            }
            else if (type == "rmb")
            {
                deleteButton(false);
            }
        }
        else if (Input.GetMouseButtonDown(1) && !Input.GetMouseButton(0)) //right click
        {
            if (type == "lmb")
            {
                deleteButton(false);
            }
            else if (type == "rmb")
            {
                deleteButton(true);
            }
        }


        /*if(Input.GetMouseButton(0) && Input.GetMouseButton(1) && type == "both") //both click
        {
            deleteButton();
        }
        else if(Input.GetMouseButtonDown(0) && !Input.GetMouseButton(1) && type == "lmb") //left click
        {
            deleteButton();
        }
        else if(Input.GetMouseButtonDown(1) && !Input.GetMouseButton(0) && type == "rmb") //right click
        {
            deleteButton();
        }*/
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="result">true is correct click, false is incorrect click</param>
    void deleteButton (bool result)
    {
        thisCollider.enabled = false;

        ChangeSize(2.0f, false);

        if (result)
        {
            gameManager.ChangeScore(1);
            //spawn +1 text
            //Vector3 posi = Camera.main.WorldToScreenPoint(transform.position);

            Instantiate(PlusOneText, transform.position, Quaternion.identity);
        }
        else
        {
            gameManager.ChangeScore(-1);
            //spawn -1 text
            //Vector3 posi = Camera.main.WorldToScreenPoint(transform.position);

            Instantiate(MinusOneText, transform.position, Quaternion.identity);
        }

        gameManager.OpenPosition(pos);

        Destroy(gameObject, 2.0f);
    }
}
